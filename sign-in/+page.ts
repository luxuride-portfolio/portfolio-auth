import { redirect } from '@sveltejs/kit';
import type { PageLoad } from './$types';

export const load: PageLoad = async ({ parent }) => {
	const session = (await parent()).session;
	if (session?.user) throw redirect(308, '/auth/sign-out');
};
