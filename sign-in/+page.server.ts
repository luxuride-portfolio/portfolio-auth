import type { Provider } from '@supabase/supabase-js';
import { redirect, type Actions } from '@sveltejs/kit';

export const actions = {
	default: async ({ locals, url }) => {
		const provider: Provider = (url.searchParams.get('provider') as Provider) ?? 'gitlab';
		const oauth = await locals.supabase.auth.signInWithOAuth({
			provider,
			options: { redirectTo: url.origin + '/auth/callback' }
		});
		throw redirect(303, oauth.data.url ?? '');
	}
} satisfies Actions;
